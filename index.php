<?php include('inc\db.php'); ?>
<?php include('inc\sql_json.php'); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Konvertor</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    </head>

<body>
  <h1>Konvertor</h1>
  
  <div id="forma">
      <form  action="convert.php" method="POST" enctype="multipart/form-data">
        <label>Iznos</label><br>
        <input type="text" name="iznos" value="" required="required"><br>
        <label>Valuta</label><br>
        <select type="text" name="valuta"  value="" required="required">
          <option value="">- Izaberi -</option>";
          <?php

          $sql = "SELECT * FROM valuta ORDER BY naziv ASC";
          $result = mysqli_query($connection,$sql) or die(mysql_error());

          if (mysqli_num_rows($result)>0) {
            while ($record = mysqli_fetch_array($result,MYSQLI_BOTH))
              echo "<option value=\"$record[id]\">$record[naziv]</option>";
          }

          ?>
        </select><br>
      <input type="radio" name="kurs" value="kupovni" checked> <p>Kupovni</p><br>
      <input type="radio" name="kurs" value="srednji"> <p>Srednji</p><br>
      <input type="radio" name="kurs" value="prodajni"> <p>Prodajni</p>
      <br>
        <input type="submit" name="convert" id="convert" value="Konvertuj">
    </form>
 </div>
</body>   
</html>