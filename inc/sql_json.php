
<?php
$sql_json = "SELECT * FROM konverzije INNER JOIN valuta ON konverzije.valuta_kod=valuta.kod ORDER BY timestamp"; 
$sql_naziv_valute = "SELECT naziv FROM valuta";
$res_json = mysqli_query($connection, $sql_json) or die(mysqli_error());
$res_naziv_valute = mysqli_query($connection, $sql_naziv_valute) or die(mysqli_error());


$konverzije = array();
$response = array();
$datumi = array();

while ($record = mysqli_fetch_array($res_json, MYSQLI_BOTH)) {
    $iznos = $record['iznos'];
    $valuta = $record['naziv'];
    $kod_valute = $record ['valuta_kod'];
    $kurs = $record['kurs'];
    $vrednost = $record['vrednost'];
    $total = $record['total'];
    $timestamp = $record['timestamp'];
    $dateTime = date('d.m.Y H:i:s', $timestamp);
    $datum = explode(" ", $dateTime)[0];

    if (!in_array($datum, $datumi, true)) {
        array_push($datumi,$datum);
    }

    $konverzije[] = array('iznos'=>$iznos, 'valuta'=>$valuta, 'kurs'=>$kurs, 'vrednost'=>$vrednost, 'total'=>$total, 'datum'=>$datum, 'valuta_kod'=>$kod_valute);
    
    for ($i=0; $i<count($datumi); $i++) {

        unset($konvertovano);
        $konvertovano = array();

        foreach ($konverzije as $konverzija) {
            if ($datumi[$i]==$konverzija['datum']) {
                array_pop($konverzija);
                array_push($konvertovano,$konverzija);
            }
        }
        $response[$i] = [
            'datum'=> $datumi[$i],
            'konverzije'=> $konvertovano
        ];
    }
}

$json = fopen('json/konverzije.json', 'w');
fwrite($json, json_encode($response,JSON_PRETTY_PRINT));
fclose($json);

?> 
